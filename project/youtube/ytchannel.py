from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class YTHandler(ContentHandler):
    """Class to handle events fired by the SAX parser

    Fills in self.videos with title, link and id for videos
    in a YT channel XML document.
    """

    def __init__(self):
        """Initialization of variables for the parser
        * inEntry: within <entry>
        * inContent: reading interesting target content (leaf strings)
        * content: target content being readed
        * title: title of the current entry
        * id: id of the current entry
        * link: link of the current entry
        * videos: list of videos (<entry> elements) in the channel,
            each video is a dictionary (title, link, id)
        """
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.id = ""
        self.link = ""
        self.videos = []
        self.image = ""
        self.channel = ""
        self.url = ""
        self.date = ""
        self.description = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'name':
                self.inContent = True
            elif name == 'uri':
                self.inContent = True
            elif name == 'published':
                self.inContent = True
            elif name == 'media:thumbnail':
                self.image = attrs.get('url')
            elif name == 'media:description':
                self.inContent = True

    def endElement(self, name):
        global videos

        if name == 'entry':
            self.inEntry = False
            self.videos.append({'link': self.link,
                                'title': self.title,
                                'id': self.id,
                                'image': self.image,
                                'channel': self.channel,
                                'url': self.url,
                                'date': self.date,
                                'description': self.description})

        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
            elif name == 'yt:videoId':
                self.id = self.content
                self.content = ""
                self.inContent = False
            elif name == 'name':
                self.channel = self.content
                self.content = ""
                self.inContent = False
            elif name == 'uri':
                self.url = self.content
                self.content = ""
                self.inContent = False
            elif name == 'published':
                self.date = self.content
                self.content = ""
                self.inContent = False
            elif name == 'media:description':
                self.description = self.content
                self.content = ""
                self.inContent = False

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class YTChannel:
    """Class to get videos in a YouTube channel.

    Extracts video links and titles from the XML document for a YT channel.
    The list of videos found can be retrieved lated by calling videos()
    """

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def videos (self):

        return self.handler.videos
