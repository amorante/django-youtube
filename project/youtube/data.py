"""Data to keep between HTTP requests (app state)

* selected: list of selected videos
* selectable: list of selectable videos
"""

selected = []
selectable = []

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Django YouTube</h1>
    <h2>Selected</h2>
      <ul>
      {selected}
      </ul>
    <h2>Selectable</h2>
      <ul>
      {selectable}
      </ul>
  </body>
</html>
"""

VIDEO = """
      <li>
        <form action='/' method='post'>
          <a href='{id}'>{title}</a>
          <input type='hidden' name='id' value='{id}'>
          <input type='hidden' name='csrfmiddlewaretoken' value='{token}'>
          <input type='hidden' name='{name}' value='True'> 
          <input type='submit' value='{action}'>
        </form>
      </li>
"""

SELECTED = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <p>Return to the <a href='/'>main page.</a></p>
        <p>Title: <a href='{video[link]}'>{video[title]}</a></p>
        <p>Thumbnail: <a href='{video[link]}'><img src={video[image]}></a></p>
        <p>Channel: <a href='{video[url]}'>{video[channel]}</a></p>
        <p>Date: {video[date]}</p>
        <p>Description: {video[description]}</p>
    </body>
</html>
"""

SELECTABLE = """
<!DOCTYPE html>
<html lang="en">
    <body>
        <p>Return to the <a href='/'>main page.</a></p>
        <p>Select the video to see the details.</p>
    </body>
</html>
"""
